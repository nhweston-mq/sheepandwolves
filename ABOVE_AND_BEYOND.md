# "Above-and-beyond" Discussion

My "above-and-beyond" implementation is included in the `TerrainGen` class, which generates terrain upon instantiation.
`Grid` then assigns the respective `Terrain` to each `Cell` using the `get(int, int)` method in `TerrainGen`.

My implementation can be considered as two main components:
1. **Fractal generation**: Generates two diamond-square fractals representing the moisture and vegetation levels across
the `Grid`.
2. **Terrain assignment**: Assigns the four different `Terrain` types across the `Grid` based on the fractals.

## Fractal Generation

`TerrainGen` begins by generating two fractal maps, one representing moisture and the other vegetation. These fractal
maps are simply tables \(a two-dimensional array specifically\) of values corresponding the table of `Cell` instances
in `Grid`. This idea of overlapping fractals was largely inspired from *Civilization V* and *Civilization VI* \(of
which I have studied the map generation scripts\), which generate independent fractals to represent height, temperature
and precipitation.

The terrain assignment component of `TerrainGen` deals with specifics of *how* `Terrain` is assigned from the fractal
values. For now, it suffices to know that the four `Terrain` types are considered in terms of having either low or high
moisture, and low or high vegetation. My idea was to have the fractal values correspond to the `Terrain` types as
follows:

|                     | **Low moisture** | **High moisture** |
| ------------------- | :--------------: | :---------------: |
| **High vegetation** | Grass            | Trees             |
| **Low vegetation**  | Rock             | Dirt              |

### Overview of the diamond-square algorithm

The fractals themselves are generated using the diamond-square algorithm as specified by Wikipedia \[1\]. The algorithm
is best defined recursively, although I implemented it as a loop.

The algorithm thus proceeds as follows:
1. Starting with a square, set the four corners to a randomly generated value.
2. Assign the centre of the square the mean of the four corner values plus a randomly generated increment. *\(This step
is referred to as the diamond step.\)*
4. For each midpoint of the square's edges, assign this point the average of the adjacent values plus a randomly
generated increment. 'Adjacent' refers to values half the square's side length away orthogonally. *\(This step is
referred to as the square step.\)*
5. The newly assigned points form four new squares. Recursively, repeat steps 2-5 on each of these new squares.

Note that in Step 4, adjacent values outside the square in question are considered. Thus there are four values averaged
unless the midpoint is on the edge of the original square \(in which case there are three\). Also note that midpoints
not on the edge of the original square actually lie on the edges of two squares. It would be redundant to assign values
to these points twice. An optimised recursive implementation would need to find a way to avoid this. This was a
consideration in my decision to use a loop instead of recursion.

### Applying the algorithm to a grid

The diamond-square algorithm is often applied in a continuous context \(i.e. where there is not an explicit 'grid'\),
such as in the generation of terrain heightmaps. The recursion can be repeated as many times as is necessary to achieve
a desired level of detail. However, in this discrete application, it's necessary to ensure that the `Grid` is filled
completely and optimally. To this end, there is an additional restraint placed upon the 2D array, which is that the
number of elements in each row and column must be of the form 2^*n* + 1, where *n* is a natural number. This ensures
that no non-square rectangles are formed due to an uneven subdivision in step 5. We are actually looking to avoid
*even* numbers, since there is one row or column that is shared by both sides of the subdivision. Every subdivision of
a square of this form will lead to four more squares of these form \(with *n* decreased by one\) until the final
recursion where the squares have side length of three.

The implication of this is that the fractals can only be generated on a 2D array of certain dimensions. Hence, the
fractals are generated on the smallest possible square 2D array with dimensions of the form 2^*n* + 1 that extend to or
beyond the dimensions of the `Grid`. In the case of a 20x20 `Grid`, `TerrainGen` will generate on a 33x33 2D array.
Upon completion of the algorithm, the array is truncated to the appropriate dimensions.

### Implementing the algorithm using loops

The loop implementation of the diamond-square algorithm I have used simply iterates through each scale, i.e.
subdivision side length. Within each of these iterations, the method iterates through each square centre for the
diamond step, and each midpoint for the square step. Given that `scale` is the side length of the squares, the square
centres can be found with a nested `for` loop, starting `x` and `y` at `scale/2`, and adding `scale` to both at each
iteration. The nested loop for the midpoints was trickier, because the midpoints form an 'offset' pattern. `x` \(the
outer loop\), starts at `0` and increases by `scale/2` at each iteration. These iterations alternate between starting
the inner `y` loop at `0` and `scale/2`. At the end of each inner iteration, `scale` is added to `y`.

I have also included loops to access the corners of a square and the midpoint adjacencies, involving a `for` loop from
`0` to `3`. Here, I will refer to the loop variable `i`. For the corner loop, we consider `i` in its binary
representation, with two digits. The right digit we treat as representing whether the corner is on the left or right,
i.e. whether we add or subtract from the `x` coordinate, and the left digit we treat as representing whether the corner
should be on the top or bottom, i.e. whether we add or subtract from the `y` coordinate. This binary interpretation can
be achieved through the use of the mod operator and integer division, both by `2`. This allows us to iterate through
every corner of a square in a single loop.

Using a single loop to access the adjacencies was somewhat more difficult. I observed that each adjacency involves
adding or subtracting from one dimension but keeping the other the same. Using `(i+1)%2` for the `x` coordinate and
`i%2` for `y`, we are changing `x` when `i` is even, and changing `y` when `i` is odd. To determine whether the
coordinate should be added or subtracted from, multiply the modulo by `i-1` for `x` and `i-2` for `y`. The table below
demonstrates how this works:

|  `i` | `(i+1)%2` | `i-1` |       `dx` | `i%2` | `i-2` |       `dy` |
| ---: | --------: | ----: | ---------: | ----: | ----: | ---------: |
|  `0` |       `1` |  `-1` | `-scale/2` |   `0` |  `-2` |        `0` |
|  `1` |       `0` |   `0` |        `0` |   `1` |  `-1` | `-scale/2` |
|  `2` |       `1` |   `1` |  `scale/2` |   `0` |   `0` |        `0` |
|  `3` |       `0` |   `2` |        `0` |   `1` |   `1` | `-scale/2` |

### The size of the increment

Another consideration in implementing the diamond-square algorithm is how the maximum magnitudes of the random
increments should decrease after each iteration. There are various approaches:
* ***Subtract* a constant value from the maximum magnitude at each iteration** \[2\]. While I did not attempt this
approach, I suspect it would lead to a much less smooth fractal than other approaches. It also runs into the potential
problem of having a negative increment limit \(which would either cause an exception or effectively *increase* the
increment limit beyond a certain point\).
* ***Multiply* a constant value from the maximum magnitude at each iteration** \[3\]. This is the approach I ended up
using. I have `TerrainGen` set up so this value can be set to anything \(the one actually used is specified as a static
variable in `Grid`\). The closer the value is to `1`, the rougher the fractal. The closer it is to `0`, the smoother
the fractal.
* **Set the increment limit for iteration *n* to (1/2)^(*kn*).** I came up with this idea myself, and used it
initially. This is similar to the multiplicative approach, but causes the increment to shrink much more quickly or
slowly depending on *k*. I eventually conceded that the multiplicative approach generates more realistic terrain than
this one.

## Terrain Assignment

With the two fractals generated, `TerrainGen` then proceeds to use these fractals to assign `Terrain` types to `Cell`
indices. The diamond-square fractal tends to be rather inconsistent in the distribution of values, so I wanted the
fractal values to be applied *relatively*. I also wanted to maintain the even distribution of `Terrain` types \(25% of
each). This led me to the idea of ranking `Cell` indices by 'suitability' for a given terrain, and then assigning the
given `Terrain` to the top 25% of `Cell` indices. The original specifications for my idea was this:
1. Define 'quota' as 25% of all `Cell` indices in the `Grid`.
2. Create a `PriorityQueue` of all `Cell` indices ranked by the product of the moisture and vegetation fractal values.
3. Poll the first quota of `Cell` indices and assign them `TREES`.
4. Resort the `PriorityQueue` by the vegetation fractal values.
5. Poll the first quota of `Cell` indices and assign them `GRASS`.
6. Resort the `PriorityQueue` by the moisture fractal values.
7. Poll the first quota of `Cell` indices and assign them `DIRT`.
8. Assign all remaining `Cell` indices `ROCK`.

I created a `Comparator`, `TerrainScoreComparator`, to order the `PriorityQueue`.

This procedure contained a few oversights which I became aware of while working on the code.

### Calculating the suitability for `TREES`

Step 2 in the original procedure is an oversight. I initially failed to consider that this would only be feasible if
all fractal values were always positive. This is obviously not the case with my implementation of the diamond-square
algorithm. An index with a very low \(i.e. very negative\) moisture value and a very low vegetation value would return
a very high suitability score for `TREES`. It became clear to me that this was not an appropriate way to be scoring the
suitability of `TREES`. I considered using the geometric mean \(i.e. the square root of the product\), but this still
does not account for the problem of negative numbers. I eventually decided upon using the *sum* of the moisture and
vegetation values, which avoids the problems associated with products.

### Serial and parallel terrain assignment

I soon realised that assigning `GRASS` before `DIRT` was a subtle oversight. In assigning `GRASS` first, I failed to
consider that there may be indices with a vegetation value high enough to be within the range of eligible indices to
assign to, but with a much higher moisture value. In this case, it would make more sense to assign `DIRT` to this
index, but my original procedure was blind to this because all assignments for a particular `Terrain` were *in series*.

My next thought was to assign `GRASS` and `DIRT` *in parallel*, such that an index with a higher moisture value will be
considered for `DIRT` before being considered for `GRASS`, and vice versa. To this end, I set up two `PriorityQueue`
instances that were peeked together. The index with the higher respective fractal value would be polled and assigned.
If an index had already been assigned the other `Terrain`, it would simply be skipped.

My next thought was to have *all four* `Terrain` types assigned in parallel. In hindsight, I'm not sure why I thought
this was a good idea. I created a nested class, `TerrainScoreNode`, which contains an index-terrain pair. This allowed
me to use a single `PriorityQueue`, rather than multiple. I noticed that this approach caused the `Terrain` to appear
less realistic, and for the square 'creases' of the diamond-square algorithm to become more prominent. I realised a
large part of the problem was that the distribution of suitability scores for `TREES` was always going to be very
different to that of `GRASS` and `DIRT`. This is why I considered using the geometric mean at one point \(but as
previously discussed, this still doesn't address negative values\).

Finally, after learning about the `Collection` interface, I created a method that would assign any number of `Terrain`
types in parallel. I reverted to my original approach of assigning `TREES` independently, but then assigning `GRASS`
and `DIRT` in parallel.

## References

1. https://en.wikipedia.org/wiki/Diamond-square_algorithm
2. https://github.com/jmecom/procedural-terrain/blob/master/js/terrain_generator.js
3. https://github.com/mlepage/heightmap/blob/master/heightmap.lua