/*
 * COMP229 A1
 * Nicholas Weston (44882017)
 */

package sheepandwolves;

import javax.swing.*;
import java.awt.*;

/**
 * <p>The main class for the <i>Sheep and Wolves</i> applet. This implementation uses the {@link javax.swing} and
 * {@link java.awt} packages to render the output graphically. Specifically, this main class is a subclass of
 * {@link JFrame} and uses {@link Runnable} as an interface. A nested class, itself a subclass of {@link JPanel} is
 * used as the content frame (see {@link #setContentPane(Container)} to represent the interior of the applet window.
 * This nested class has an overriden <code>paint(Graphics)</code> function (see {@link JPanel#paint(Graphics)}),
 * which obtains the graphical output by calling a respective {@link Grid} object to draw on its {@link Graphics}
 * parameter.</p>
 *
 * <p>This class will continue sending user input (i.e. the mouse position) to its respective {@link Grid} object. If
 * it detects a change in the state of the <code>Grid</code>, it will call {@link #repaint()} to update the entire
 * graphical output.</p>
 *
 * @author  Nicholas Weston
 * @see     JFrame
 * @see     JPanel
 * @see     Runnable
 */
public class Main extends JFrame implements Runnable {

    /**
     * The width of the applet canvas.
     */
    protected static final int width = 1280;

    /**
     * The height of the applet canvas.
     */
    protected static final int height = 720;

    /**
     * The background colour of the canvas.
     */
    protected static final Color colBg = Color.BLACK;

    /**
     * The colour of the outline of each {@link Cell}.
     */
    protected static final Color colStroke = Color.WHITE;

    /**
     * The colour of the square overlayed on a highlighted {@link Cell}. Note that the transparency of the highlight
     * is represented in the alpha value of the {@link Color}.
     */
    protected static final Color colHighlight = new Color(255, 255, 255, 128);

    /**
     * The main function when the applet is run. Creates a new instance of this class, and then calls {@link #run()}
     * on it.
     *
     * @param args  unused.
     */
    public static void main(String[] args) {
        Main main = new Main();
        main.run();
    }

    private Grid grid;

    /**
     * Private nested class representing the canvas, i.e. the interior of the window. Simply a {@link JPanel} with a
     * predefined dimensions (specifically the static {@link Main#width} and {@link Main#height} fields), along with an
     * overriden {@link #paint(Graphics)} function.
     *
     * @see     java.awt.Canvas
     */
    private class Canvas extends JPanel {

        /**
         * Creates a new <code>Canvas</code> with dimensions as defined by the static {@link Main#width} and
         * {@link Main#height}.
         */
        private Canvas() {
            setPreferredSize(new Dimension(width, height));
        }

        /**
         * Paints the <code>Canvas</code> by calling {@link Grid#paint(Graphics)}.
         *
         * @param g     the {@link Graphics} to paint to.
         */
        @Override
        public void paint(Graphics g) {
            grid.paint(g);
        }

    }

    /**
     * Creates a new instance of <code>Main</code>, representing the applet itself. Called once by
     * {@link #main(String[])}.
     *
     * @see     #setDefaultCloseOperation(int)
     * @see     #setContentPane(Container)
     * @see     #pack()
     * @see     #setVisible(boolean)
     */
    private Main() {
        grid = new Grid();
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setContentPane(new Canvas());
        pack();
        setVisible(true);
    }

    /**
     * <p>Runs the applet. Continuously sends the mouse position to the {@link Grid#input(int, int)} method of the
     * <code>Grid</code> object displayed by the applet while the mouse is within the window. (Note that the mouse
     * position is relative to the canvas, not the entire window itself.) Also continuously checks whether the
     * <code>Grid</code> has changed, and if so, calls {@link #repaint()}.</p>
     *
     * @see     Runnable#run()
     * @see     JPanel#getMousePosition()
     */
    @Override
    public void run() {
        while (isActive()) {
            Point mousePos = getContentPane().getMousePosition();
            if (mousePos != null) {
                grid.input(mousePos.x, mousePos.y);
            }
            if (grid.isChanged()) {
                repaint();
            }
        }
    }

}