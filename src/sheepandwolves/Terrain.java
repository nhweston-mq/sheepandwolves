/*
 * COMP229 A1
 * Nicholas Weston (44882017)
 */

package sheepandwolves;

import java.awt.*;
import java.util.Hashtable;
import java.util.Map;
import java.util.Random;

/**
 * Enum representing the different terrain types of a {@link Cell}.
 */
public enum Terrain {

    ROCK    (0, new Color(0xC0C0C0)),
    DIRT    (1, new Color(0x804000)),
    GRASS   (2, new Color(0x80FF00)),
    TREES   (3, new Color(0x008040));

    private static Map<Integer, Terrain> map = new Hashtable<>();
    private static Random random = new Random();
    private static Terrain[] array;

    static {
        for (Terrain terrain : Terrain.values()) {
            map.put(terrain.id, terrain);
        }
        array = new Terrain[map.values().size()];
        array = map.values().toArray(array);
    }

    /**
     * Converts an <code>int</code> into a <code>Terrain</code>, or <code>null</code> if such a <code>Terrain</code>
     * does not exist.
     *
     * @param id    the <code>int</code> to convert.
     * @return      the <code>Terrain</code> corresponding to the <code>int</code>.
     */
    public static Terrain getById(int id) {
        return map.get(id);
    }

    /**
     * Returns a random <code>Terrain</code>.
     *
     * @return  a random <code>Terrain</code>.
     */
    public static Terrain random() {
        return array[random.nextInt(array.length)];
    }

    private int id;
    private Color color;

    /**
     * Creates a new <code>Terrain</code> enum, assigning the specified <code>int</code> as its associated identifier
     * for {@link #getById(int)}, and the specified {@link Color} to render it as.
     *
     * @param id        the desired identifier to associate this <code>Terrain</code> to.
     * @param color     the color to render a {@link Cell} with this <code>Terrain</code> as.
     */
    Terrain(int id, Color color) {
        this.id = id;
        this.color = color;
    }

    /**
     * Returns the {@link Color} to render this as.
     *
     * @return  the <code>Color</code> to render a {@link Cell} with this <code>Terrain</code> as.
     */
    public Color getColor() {
        return this.color;
    }

}