/*
 * COMP229 A1
 * Nicholas Weston (44882017)
 */

package sheepandwolves;

import java.awt.*;

/**
 * Represents a dirt {@link CreditCell} in a {@link CreditGrid}.
 *
 * @author  Nicholas Weston
 */
@Deprecated
public class Rock extends CreditCell {

    /**
     * The colour of rocks terrain.
     */
    protected static final Color color = new Color(192, 192, 192);

    /**
     * Creates a new terrain at the specified position, interpreted as the top-left corner of the <code>Rock</code>.
     *
     * @param posX  the desired x-position of the <code>Rock</code>.
     * @param posY  the desired y-position of the <code>Rock</code>.
     */
    public Rock(int posX, int posY) {
        super(posX, posY);
    }

    /**
     * Method to draw the <code>Rock</code> object on the specified {@link Graphics} with the specified size.
     *
     * @param g     the <code>Graphics</code> to draw the <code>Rock</code> on.
     * @param size  the side length of the <code>Rock</code> to be drawn.
     */
    @Override
    public void paint(Graphics g, int size) {
        g.setColor(color);
        g.fillRect(getPosX(), getPosY(), size, size);
        g.setColor(Main.colStroke);
        g.drawRect(getPosX(), getPosY(), size, size);
    }

}
