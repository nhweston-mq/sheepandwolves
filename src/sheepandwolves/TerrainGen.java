/*
 * COMP229 A1
 * Nicholas Weston (44882017)
 */

package sheepandwolves;

import java.util.*;

/**
 * <p>Class used to randomly generate {@link Terrain} for the <em>Sheep and Wolves</em> applet. This class will not
 * assign values to {@link Cell} instances themselves, but can be obtained from {@link #get(int, int)}.
 *
 * <p>This class uses the diamond-square algorithm to generate fractals representing moisture and vegetation. These
 * values correspond to the <code>Terrain</code> enums as follows:</p>
 * <ul>
 *     <li><code>ROCK</code>: Low moisture, low vegetation.</li>
 *     <li><code>DIRT</code>: High moisture, low vegetation.</li>
 *     <li><code>GRASS</code>: Low moisture, high vegetation.</li>
 *     <li><code>TREES</code>: High moisture, high vegetation.</li>
 * </ul>
 *
 * <p>After generating the fractal, <code>Cell</code> indices are assigned via {@link PriorityQueue} instances. These
 * <code>PriorityQueue</code> instances contain nodes, each representing an index-terrain pair. The pairs are ranked
 * by a score representing the 'suitability' of the <code>Cell</code> index for the <code>Terrain</code>. The highest
 * scoring pairs are assigned first, until a quota of indices have been assigned the <code>Terrain</code> in
 * question. <code>TREES</code> are assigned first, following by <code>GRASS</code> and <code>DIRT</code> in parallel,
 * followed by the filling of the remaining <code>Cell</code> instances with <code>ROCK</code>.</p>
 *
 */
public class TerrainGen {

    private static Map<Terrain, Double> proportions = new Hashtable<>();

    static {
        proportions.put(Terrain.ROCK, 0.25);
        proportions.put(Terrain.DIRT, 0.25);
        proportions.put(Terrain.GRASS, 0.25);
        proportions.put(Terrain.TREES, 0.25);
    }

    private Random random;
    private int dimX, dimY;
    private double[][] fracFlora;
    private double[][] fracWater;
    private Terrain[][] terrains;
    private double incrLimitRatio;

    /**
     * Stores a {@link Cell} index and a {@link Terrain}. These are used in by a {@link PriorityQueue} in
     */
    private class TerrainScoreNode {

        private Integer index;
        private Terrain terrain;

        private TerrainScoreNode(int index, Terrain terrain) {
            this.index = index;
            this.terrain = terrain;
        }

        private Integer getIndex() {
            return index;
        }

        private Terrain getTerrain() {
            return terrain;
        }

    }

    /**
     * Comparator to compare the fractal values of the {@link Cell} indices in the specified {@link TerrainScoreNode}
     * instances, using the score for the respective {@link Terrain} types in the <code>TerrainScoreNode</code>.
     */
    private class TerrainScoreComparator implements Comparator<TerrainScoreNode> {

        /**
         * Compares the fractal values of the {@link Cell} indices in the specified {@link TerrainScoreNode} instances,
         * using the score for the respective {@link Terrain} types in the <code>TerrainScoreNode</code>.
         *
         * @param i1    a <code>Cell</code> index to compare.
         * @param i2    a <code>Cell</code> index to compare.
         * @return      <ul>
         *                  <li><code>1</code> if <code>i1</code> has a more suitable <code>Cell</code> index than
         *                  <code>i2</code>for the {@link Terrain} of this <code>TerrainScoreComparator</code>.</li>
         *                  <li><code>0</code> if <code>i1</code> and <code>i2</code> have equally suitable
         *                  <code>Cell</code> indices.</li>
         *                  <li><code>-1</code> if <code>i1</code> has a less suitable <code>Cell</code> index than
         *                  <code>i2</code>.</li>
         *              </ul>
         */
        public int compare(TerrainScoreNode i1, TerrainScoreNode i2) {
            double score1 = getScore(i1.getIndex(), i1.getTerrain());
            double score2 = getScore(i2.getIndex(), i2.getTerrain());
            return Double.compare(score1, score2);
        }

    }

    /**
     * <p>Creates a <code>TerrainGen</code> for the specified {@link Grid} and the specified increment limit ratio.</p>
     *
     * <p>The increment limit ratio is the ratio by which the range of the random increment is changed upon each
     * iteration of the diamond-square algorithm. This should be a value between <code>0.0</code> and <code>1.0</code>.
     * Values outside this range may have unexpected results. This roughly corresponds to the smoothness of the terrain
     * generation. Lower values will result in smoother terrain; higher values in rougher terrain.</p>
     *
     * @param grid              the <code>Grid</code> to generate terrain for.
     * @param incrLimitRatio    the desired increment limit ratio for the diamond-square algorithm.
     */
    public TerrainGen(Grid grid, double incrLimitRatio) {
        this.incrLimitRatio = incrLimitRatio;
        dimX = grid.getDimX();
        dimY = grid.getDimY();
        random = new Random();
        fracWater = genFractal(dimX, dimY);
        fracFlora = genFractal(dimX, dimY);
        terrains = new Terrain[dimX][dimY];
        HashSet<Integer> unassigned = new HashSet<>();
        for (int i=0; i<dimX*dimY; i++) {
            unassigned.add(i);
        }
        Collection<Terrain> terrainsToAdd;
        // Assign trees first.
        terrainsToAdd = new HashSet<>();
        terrainsToAdd.add(Terrain.TREES);
        assignByFractal(unassigned, terrainsToAdd);
        // Then assign grass and dirt.
        terrainsToAdd = new HashSet<>();
        terrainsToAdd.add(Terrain.GRASS);
        terrainsToAdd.add(Terrain.DIRT);
        assignByFractal(unassigned, terrainsToAdd);
        // Fill remainder with rock.
        setTerrain(unassigned, Terrain.ROCK);
    }

    /**
     * Returns the {@link Terrain} of the specified {@link Cell} index.
     *
     * @param i     the desired <code>Cell</code> index.
     * @return      the <code>Terrain</code> of the <code>Cell</code>.
     */
    public Terrain get(int i) {
        return get(i%dimX, i/dimX);
    }

    /**
     * Returns the {@link Terrain} of the {@link Cell} at the specified coordinates.
     *
     * @param x     the x-position of the desired <code>Cell</code>.
     * @param y     the y-position of the desired <code>Cell</code>.
     * @return      the <code>Terrain</code> of the <code>Cell</code>.
     */
    public Terrain get(int x, int y) {
        try {
            return terrains[x][y];
        }
        catch (ArrayIndexOutOfBoundsException e) {
            return null;
        }
    }

    /**
     * <p>Generates a fractal using the diamond-square algorithm. This is a recursive algorithm (implemented here as a
     * loop), taking a square array and its corner values to generate values for the edge mid-points and the centre of
     * the square.</p>
     *
     * <ol>
     *     <li>The centre of the square is set to the average of the four corners, plus a random increment.</li>
     *     <li>Each edge midpoint is set to the average of the four adjacent values (or three if the value is on the
     *     edge of the initial square). Note that <em>adjacent</em> refers to points half the side length of the square
     *     away orthogonally.</li>
     *     <li>The process is repeated for the four new squares whose corner values have just been set.</li>
     *     <li>This recursion continues until the initial square is filled out.</li>
     * </ol>
     *
     * <p>Note that this method generates a fractal for a square of side length of the form n^2+1, since this avoids
     * dealing with fractional side lengths at any point in the recursive process. The resulting array is then
     * truncated to the desired dimensions.</p>
     *
     * @param dimX  the number of columns to generate.
     * @param dimY  the number of rows to generate.
     * @return      the array of values generated.
     */
    private double[][] genFractal(int dimX, int dimY) {
        // Find the smallest integer of form n^2+1 larger than both dimX and dimY.
        double minPowOfTwo = Math.pow(2.0, Math.ceil(Math.max(Math.log(dimX-1), Math.log(dimY-1)) / Math.log(2)));
        int dim = (new Double(minPowOfTwo)).intValue() + 1;
        double[][] result = new double[dim][dim];
        double incrLimit = 1.0;
        // Initialise corners
        for (int i=0; i<4; i++) {
            result[(i%2)*(dim-1)][(i/2)*(dim-1)] = genFracIncr(incrLimit);
        }
        // Diamond-square
        for (int scale=dim-1; scale>1; scale=scale/2) {
            incrLimit = incrLimit * incrLimitRatio;
            fracDiamond(result, scale, incrLimit);
            fracSquare(result, scale, incrLimit);
        }
        // Truncate array
        double[][] temp = result;
        result = new double[dimX][dimY];
        for (int x=0; x<result.length; x++) {
            result[x] = Arrays.copyOf(temp[x], dimY);
        }
        return result;
    }

    /**
     * Utility method for performing the diamond step of the diamond-square algorithm.
     *
     * @param arr           the array the fractal is generated in.
     * @param scale         the side length of the squares in this iteration.
     * @param incrLimit     the maximum magnitude of the randomly generated increments.
     */
    private void fracDiamond(double[][] arr, int scale, double incrLimit) {
        for (int x=scale/2; x<arr.length; x=x+scale) {
            for (int y=scale/2; y<arr[0].length; y=y+scale) {
                double sum = 0;
                int nPoints = 0;
                for (int iCorner=0; iCorner<4; iCorner++) {
                    // Center is sum of corners
                    int xCorner = x-scale/2+(iCorner%2)*2*(scale/2);
                    int yCorner = y-scale/2+(iCorner/2)*2*(scale/2);
                    if (xCorner >= 0 && xCorner < arr.length && yCorner >= 0 && yCorner < arr[0].length) {
                        sum = sum + arr[xCorner][yCorner];
                        nPoints++;
                    }
                }
                arr[x][y] = sum/nPoints + genFracIncr(incrLimit);
            }
        }
    }

    /**
     * Utility method for performing the square step of the diamond-square algorithm.
     *
     * @param arr           the array the fractal is generated in.
     * @param scale         the side length of the squares in this iteration.
     * @param incrLimit     the maximum magnitude of the randomly generated increments.
     */
    private void fracSquare(double[][] arr, int scale, double incrLimit) {
        for (int x=0; x<arr.length; x=x+scale/2) {
            int startY = 0;
            if (x % scale == 0) {
                startY = scale/2;
            }
            for (int y=startY; y<arr[0].length; y=y+scale) {
                double sum = 0;
                int nPoints = 0;
                for (int iAdj=0; iAdj<4; iAdj++) {
                    // Center is sum of adjacencies
                    int xAdj = x + (scale/2) * ((iAdj+1)%2) * (iAdj-1);
                    int yAdj = y + (scale/2) * (iAdj%2) * (iAdj-2);
                    if (xAdj >= 0 && xAdj < arr.length && yAdj >= 0 && yAdj < arr[0].length) {
                        sum = sum + arr[xAdj][yAdj];
                        nPoints++;
                    }
                }
                arr[x][y] = sum/nPoints + genFracIncr(incrLimit);
            }
        }
    }

    /**
     * Generates a random number between <code>-incrLimit</code> and <code>incrLimit</code>. Used to generate random
     * increments for {@link #genFractal(int, int)}.
     *
     * @param incrLimit     the limit of magnitude of the random value generated.
     * @return              a random value within the specified limit.
     */
    private double genFracIncr(double incrLimit) {
        return incrLimit * (2*random.nextDouble() - 1);
    }

    /**
     * <p>Sets the {@link Terrain} of {@link Cell} indices in accordance with the moisture and vegetation fractals.
     * Only <code>Terrain</code> types and <code>Cell</code> indices in the specified {@link Collection} instances. A
     * {@link PriorityQueue} is used to list each index-terrain pair in a {@link TerrainScoreNode}, using
     * {@link TerrainScoreComparator} as the {@link Comparator}.</p>
     *
     * <p>Note that the <code>unassigned</code> parameter will be updated by this method, removing each
     * <code>Cell</code> index that has been assigned a <code>Terrain</code>.</p>
     *
     * @param terrainsToAdd     {@link Collection} listing the terrains to assign.
     * @param unassigned        a list of {@link Cell} indices to consider in the assignment.
     * @see                     #getScore(int, int, Terrain)
     */
    private void assignByFractal(Collection<Integer> unassigned, Collection<Terrain> terrainsToAdd) {
        // Counts how many more Cells of a Terrain type need to be assigned.
        Map<Terrain, Integer> terrainCounts = new Hashtable<>();
        // Rankings of cell-terrain pairs.
        PriorityQueue<TerrainScoreNode> pq = new PriorityQueue<>(new TerrainScoreComparator());
        for (Terrain terrain : terrainsToAdd) {
            // Get number of Cells to assign Terrain.
            Double terrainCount = dimX * dimY * proportions.get(terrain);
            terrainCounts.put(terrain, terrainCount.intValue());
            for (int i : unassigned) {
                // Add cell-terrain pairs to PriorityQueue.
                pq.add(new TerrainScoreNode(i, terrain));
            }
        }
        int nTerrains = terrainsToAdd.size();
        int nFinished = 0;
        // Assign Terrains to Cells.
        while (nFinished < nTerrains && !pq.isEmpty()) {
            TerrainScoreNode node = pq.poll();
            int index = node.getIndex();
            Terrain terrain = node.getTerrain();
            Integer terrainCount = terrainCounts.get(terrain);
            if (get(index) == null && terrainCount > 0) {
                // Cell is unassigned and can be assigned Terrain. Assign.
                setTerrain(index, terrain);
                terrainCount--;
                terrainCounts.put(terrain, terrainCount);
                unassigned.remove(index);
                if (terrainCount == 0) {
                    // All Cells have been assigned for this Terrain.
                    nFinished++;
                }
            }
        }
    }

    /**
     * Sets the {@link Cell} index specified to the specified {@link Terrain}.
     *
     * @param id        the index of the desired <code>Cell</code>.
     * @param terrain   the desired <code>Terrain</code> to set to.
     */
    private void setTerrain(int id, Terrain terrain) {
        setTerrain(id%dimX, id/dimX, terrain);
    }

    /**
     * Sets the {@link Cell} index at the specified coordinates to the specified {@link Terrain}.
     *
     * @param x         the x-coordinate of the desired <code>Cell</code>.
     * @param y         the y-coordinate of the desired <code>Cell</code>.
     * @param terrain   the desired <code>Terrain</code>.
     */
    private void setTerrain(int x, int y, Terrain terrain) {
        terrains[x][y] = terrain;
    }

    /**
     * Sets the {@link Cell} indices in the given {@link Collection} to the specified {@link Terrain}.
     *
     * @param unassigned    a list of <code>Cell</code> indices to assign the <code>Terrain</code> to.
     * @param terrain       the desired <code>Terrain</code>.
     */
    private void setTerrain(Collection<Integer> unassigned, Terrain terrain) {
        for (Integer index : unassigned) {
            setTerrain(index, terrain);
        }
    }

    /**
     * <p>Gets the fractal terrain score of the specified {@link Cell} index for the specified {@link Terrain}.</p>
     *
     * <p>See {@link #getScore(int, int, Terrain)} for details on how these values are determined.</p>
     *
     * @param id        the index of the desired <code>Cell</code>.
     * @param terrain   the desired <code>Terrain</code>.
     * @return          the suitability of the <code>Cell</code> for the <code>Terrain</code>.
     */
    private double getScore(int id, Terrain terrain) {
        return getScore(id%dimX, id/dimX, terrain);
    }

    /**
     * <p>Gets the fractal terrain score of the {@link Cell} at the specified coordinates for the specified
     * {@link Terrain}. The values are calculated as follows:</p>
     *
     * <ul>
     *     <li><code>ROCK</code>: Always zero. <code>ROCK</code> is essentially what's assigned to the 'leftover'
     *     <code>Cell</code> indices.</li>
     *     <li><code>DIRT</code>: The moisture fractal value.</li>
     *     <li><code>GRASS</code>: The vegetation fractal value.</li>
     *     <li><code>TREES</code>: The sum of moisture and vegetation fractal values.</li>
     * </ul>
     *
     * @param x         the x-coordinate of the desired <code>Cell</code>.
     * @param y         the y-coordinate of the desired <code>Cell</code>.
     * @param terrain   the desired <code>Terrain</code>.
     * @return          the suitability of the <code>Cell</code> for the <code>Terrain</code>.
     */
    private double getScore(int x, int y, Terrain terrain) {
        switch (terrain) {
            case DIRT:
                return fracWater[x][y];
            case GRASS:
                return fracFlora[x][y];
            case TREES:
                return fracFlora[x][y] + fracWater[x][y];
        }
        return 0;
    }

}
