/*
 * COMP229 A1
 * Nicholas Weston (44882017)
 */

package sheepandwolves;

import java.awt.*;

/**
 * Represents the grid that is displayed in the <em>Sheep and Wolves</em> applet, providing methods for its I/O and
 * functionality.
 *
 * @author  Nicholas Weston
 * @see     Cell
 */
public class Grid {

    /**
     * The default number of columns in a grid.
     */
    protected static final int defDimX = 20;

    /**
     * The default number of rows in a grid.
     */
    protected static final int defDimY = 20;

    /**
     * The default distance between the left edge of the canvas and the grid.
     */
    protected static final int defMarginX = 10;

    /**
     * The default distance between the top edge of the canvas and the grid.
     */
    protected static final int defMarginY = 10;

    /**
     * The default increment limit ratio for the {@link TerrainGen} instance.
     *
     * @see     TerrainGen
     */
    protected static final double defIncrLimitRatio = 0.75;

    private int posX, posY;
    private int cellSize;
    private Cell[][] cells;
    private Cell active;
    private boolean changed;
    private TerrainGen terrainGen;

    /**
     * Creates a new instance of {@link Grid}. Uses the default position and dimensions, with the size of each
     * {@link Cell} such that it is maximised within the window (confined by the default margins).
     */
    public Grid() {
        setPos(defMarginX, defMarginY);
        initCells(defDimX, defDimY);
        genTerrain();
    }

    /**
     * Creates a new instance of {@link Grid} with specified dimensions, position and cell size.
     *
     * @param dimX          the number of columns in the grid.
     * @param dimY          the number of rows in the grid.
     * @param posX          the x-coordinate of the top-left corner of the grid.
     * @param posY          the y-coordinate of the top-left corner of the grid.
     * @param cellSize      the side length of each cell in pixels.
     */
    public Grid(int dimX, int dimY, int posX, int posY, int cellSize) {
        setPos(posX, posY);
        setCellSize(cellSize);
        initCells(dimX, dimY, cellSize);
    }

    /**
     * Draws the <code>Grid</code> in its current state to the specified {@link Graphics}. Calling this method will
     * cause {@link #isChanged()} to return <code>false</code> until the <code>Grid</code> is updated again.
     *
     * @param g     the {@link Graphics} to draw this to.
     */
    public void paint(Graphics g) {
        changed = false;
        g.setColor(Main.colBg);
        g.fillRect(0, 0, Main.width, Main.height);
        for (Cell[] column : cells) {
            for (Cell cell : column) {
                cell.paint(g, cellSize);
            }
        }
        g.setColor(Main.colHighlight);
        if (active != null) {
            g.fillRect(active.getPosX(), active.getPosY(), cellSize, cellSize);
        }
    }

    /**
     * Updates the <code>Grid</code> according to the specified position interpreted as the current mouse position.
     *
     * @param posX  the x-position of the mouse.
     * @param posY  the y-position of the mouse.
     */
    public void input(int posX, int posY) {
        Cell prevActive = active;
        active = getCellByPos(posX, posY);
        if (prevActive != active) {
            changed = true;
        }
    }

    /**
     * Returns the {@link Cell} at the specified position, or <code>null</code> if such a <code>Cell</code> exists.
     *
     * @param posX  the x-position of the desired <code>Cell</code>.
     * @param posY  the y-position of the desired <code>Cell</code>.
     * @return      the <code>Cell</code> at the specified position.
     */
    public Cell getCellByPos(int posX, int posY) {
        int x = (posX - this.posX) / cellSize;
        int y = (posY - this.posY) / cellSize;
        try {
            return cells[x][y];
        }
        catch (ArrayIndexOutOfBoundsException e) {
            return null;
        }
    }

    /**
     * Returns the number of columns in the <code>Grid</code>.
     *
     * @return  the number of columns in the <code>Grid</code>.
     */
    public int getDimX() {
        return cells.length;
    }

    public int getDimY() {
        return cells[0].length;
    }

    /**
     * Returns <code>true</code> if the <code>Grid</code> has changed since {@link #paint(Graphics)} was last changed,
     * <code>false</code> otherwise.
     *
     * @return  <code>true</code>if the <code>Grid</code> has changed since {@link #paint(Graphics)} was last changed,
     *          <code>false</code> otherwise.
     */
    public boolean isChanged() {
        return changed;
    }

    /**
     * Instantiates the {@link Cell} array (but not the <code>Cell</code> objects themselves) with the desired
     * dimensions. Optimises the size of the <code>Cell</code> objects in accordance with the default margins
     * ({@link #defMarginX} and {@link #defMarginY}).
     *
     * @param dimX  the desired number of columns.
     * @param dimY  the desired number of rows.
     */
    private void initCells(int dimX, int dimY) {
        dimX = Math.max(dimX, 2);
        dimY = Math.max(dimY, 2);
        int maxCellSizeX = (Main.width - 2*defMarginX) / dimX;
        int maxCellSizeY = (Main.height - 2*defMarginY) / dimY;
        setCellSize(Math.min(maxCellSizeX, maxCellSizeY));
        this.cells = new Cell[dimX][dimY];
        genCells();
    }

    /**
     * Instantiates the {@link Cell} array (but not the <code>Cell</code> objects themselves) with the desired
     * dimensions and size of the <code>Cell</code> objects.
     *
     * @param dimX      the desired number of columns.
     * @param dimY      the desired number of rows.
     * @param cellSize  the desired size of each {@link Cell}.
     */
    private void initCells(int dimX, int dimY, int cellSize) {
        dimX = Math.max(dimX, 2);
        dimY = Math.max(dimY, 2);
        setCellSize(cellSize);
        this.cells = new Cell[dimX][dimY];
        genCells();
    }

    /**
     * Instantiates each {@link Cell} in the <code>Grid</code>.
     */
    private void genCells() {
        for (int x=0; x<cells.length; x++) {
            for (int y=0; y<cells[0].length; y++) {
                cells[x][y] = new Cell(posX + x*cellSize, posY + y*cellSize);
            }
        }
    }

    /**
     * Uses an instance of {@link TerrainGen} to assign {@link Terrain} to each {@link Cell} in this <code>Grid</code>.
     */
    private void genTerrain() {
        terrainGen = new TerrainGen(this, defIncrLimitRatio);
        for (int x=0; x<cells.length; x++) {
            for (int y=0; y<cells[0].length; y++) {
                cells[x][y].setTerrain(getTerrain(x, y));
            }
        }
    }

    /**
     * Returns the {@link Terrain} of the {@link Cell} at the specified coordinates.
     *
     * @param x     the x-coordinate of the desired <code>Cell</code>.
     * @param y     the y-coordinate of the desired <code>Cell</code>.
     * @return      the <code>Terrain</code> of the <code>Cell</code>.
     */
    private Terrain getTerrain(int x, int y) {
        return terrainGen.get(x, y);
    }

    /**
     * Sets the side length of each {@link Cell} in the <code>Grid</code>. Should not be called from outside the
     * constructors.
     *
     * @param cellSize  the desired side length of each <code>Cell</code>.
     */
    private void setCellSize(int cellSize) {
        this.cellSize = cellSize;
        if (cells != null) {
            changed = true;
        }
    }

    /**
     * Sets the position of the <code>Grid</code>, interpreted as the top-left corner. Should not be called from
     * outside the constructors.
     *
     * @param posX  the desired x-position of the <code>Grid</code>.
     * @param posY  the desired y-position of the <code>Grid</code>.
     */
    private void setPos(int posX, int posY) {
        this.posX = posX;
        this.posY = posY;
        if (cells != null) {
            changed = true;
        }
    }

}