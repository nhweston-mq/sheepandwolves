/*
 * COMP229 A1
 * Nicholas Weston (44882017)
 */

package sheepandwolves;

import java.awt.*;
import java.util.Random;

/**
 * Represents a cell in a {@link Grid}, providing methods for its I/O and functionality.
 *
 * @author  Nicholas Weston
 */
public class Cell {

    private static Random random = new Random();

    private int posX, posY;
    private Terrain terrain;

    /**
     * <p>Creates a new <code>Cell</code> at the specified position, assigning a random {@link Terrain} to it.</p>
     *
     * <p><em>Note: The assignment of</em> <code>Terrain</code> <em>is for the Pass section. The original</em>
     * <code>Terrain</code> <em>is replaced by</em> {@link Grid#genTerrain()}.</p>
     *
     * @param posX  the desired x-position of the <code>Cell</code>.
     * @param posY  the desired y-position of the <code>Cell</code>.
     */
    public Cell(int posX, int posY) {
        setPos(posX, posY);
        setTerrain(Terrain.random());
    }

    /**
     * Draws the <code>Cell</code> on the specified {@link Graphics} with the specified side length.
     *
     * @param g     the <code>Graphics</code> to draw the <code>Cell</code> to.
     * @param size  the side length of the <code>Cell</code> to be drawn.
     */
    public void paint(Graphics g, int size) {
        g.setColor(terrain.getColor());
        g.fillRect(posX, posY, size, size);
        g.setColor(Main.colStroke);
        g.drawRect(posX, posY, size, size);
    }

    /**
     * Sets the <code>Cell</code> to the specified {@link Terrain}.
     *
     * @param terrain   the desired <code>Terrain</code> of the <code>Cell</code>.
     */
    public void setTerrain(Terrain terrain) {
        this.terrain = terrain;
    }

    /**
     * Returns the x-position of the top-left corner of the <code>Cell</code>.
     *
     * @return  the x-position of the <code>Cell</code>.
     */
    public int getPosX() {
        return posX;
    }

    /**
     * Returns the y-position of the top-left corner of the <code>Cell</code>.
     *
     * @return  the y-position of the <code>Cell</code>.
     */
    public int getPosY() {
        return posY;
    }

    /**
     * Sets the position of the <code>Cell</code> to the specified position, interpreted as the top-left corner. Should
     * not be called from outside the constructors.
     *
     * @param posX  the desired x-position of the <code>Cell</code>.
     * @param posY  the desired y-position of the <code>Cell</code>.
     */
    private void setPos(int posX, int posY) {
        this.posX = posX;
        this.posY = posY;
    }

}
