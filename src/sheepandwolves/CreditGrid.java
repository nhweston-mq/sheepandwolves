/*
 * COMP229 A1
 * Nicholas Weston (44882017)
 */

package sheepandwolves;

import java.awt.*;
import java.util.Random;

/**
 * Represents the grid that is displayed in the <i>Sheep and Wolves</i> applet, providing methods for its I/O and
 * functionality.
 *
 * @author  Nicholas Weston
 * @see     CreditCell
 */
@Deprecated
public class CreditGrid {

    /**
     * The default number of columns in a grid.
     */
    protected static final int defDimX = 20;

    /**
     * The default number of rows in a grid.
     */
    protected static final int defDimY = 20;

    /**
     * The default distance between the left edge of the canvas and the grid.
     */
    protected static final int defMarginX = 10;

    /**
     * The default distance between the top edge of the canvas and the grid.
     */
    protected static final int defMarginY = 10;

    private static Random random = new Random();

    private int posX, posY;
    private int cellSize;
    private CreditCell[][] cells;
    private CreditCell active;
    private boolean changed;

    /**
     * Creates a new instance of {@link CreditGrid}. Uses the default position and dimensions, with the size of each
     * {@link CreditCell} such that it is maximised within the window (confined by the default margins).
     */
    public CreditGrid() {
        setPos(defMarginX, defMarginY);
        initCells(defDimX, defDimY);
    }

    /**
     * Creates a new instance of {@link CreditGrid} with specified dimensions, position and cell size.
     *
     * @param dimX          the number of columns in the grid.
     * @param dimY          the number of rows in the grid.
     * @param posX          the x-coordinate of the top-left corner of the grid.
     * @param posY          the y-coordinate of the top-left corner of the grid.
     * @param cellSize      the side length of each cell in pixels.
     */
    public CreditGrid(int dimX, int dimY, int posX, int posY, int cellSize) {
        setPos(posX, posY);
        setCellSize(cellSize);
        initCells(dimX, dimY, cellSize);
    }

    /**
     * Draws the <code>CreditGrid</code> in its current state to the specified {@link Graphics}. Calling this method
     * will cause {@link #isChanged()} to return <code>false</code> until the <code>CreditGrid</code> is updated again.
     *
     * @param g     the {@link Graphics} to draw this to.
     */
    public void paint(Graphics g) {
        changed = false;
        g.setColor(Main.colBg);
        g.fillRect(0, 0, Main.width, Main.height);
        for (CreditCell[] column : cells) {
            for (CreditCell cell : column) {
                cell.paint(g, cellSize);
            }
        }
        g.setColor(Main.colHighlight);
        if (active != null) {
            g.fillRect(active.getPosX(), active.getPosY(), cellSize, cellSize);
        }
    }

    /**
     * Updates the <code>CreditGrid</code> according to the specified position interpreted as the current mouse
     * position.
     *
     * @param posX  the x-position of the mouse.
     * @param posY  the y-position of the mouse.
     */
    public void input(int posX, int posY) {
        CreditCell prevActive = active;
        active = getCellByPos(posX, posY);
        if (prevActive != active) {
            changed = true;
        }
    }

    /**
     * Returns the {@link CreditCell} at the specified position, or <code>null</code> if such a
     * <code>CreditCell</code> exists.
     *
     * @param posX  the x-position of the desired <code>CreditCell</code>.
     * @param posY  the y-position of the desired <code>CreditCell</code>.
     * @return      the <code>CreditCell</code> at the specified position.
     */
    public CreditCell getCellByPos(int posX, int posY) {
        int x = (posX - this.posX) / cellSize;
        int y = (posY - this.posY) / cellSize;
        try {
            return cells[x][y];
        }
        catch (ArrayIndexOutOfBoundsException e) {
            return null;
        }
    }

    /**
     * Returns the number of columns in the <code>CreditGrid</code>.
     *
     * @return  the number of columns in the <code>CreditGrid</code>.
     */
    public int getDimX() {
        return cells.length;
    }

    /**
     * Returns the number of rows in the <code>CreditGrid</code>.
     *
     * @return  the number of <code>CreditGrid</code>.
     */
    public int getDimY() {
        return cells[0].length;
    }

    /**
     * Returns <code>true</code> if the <code>CreditGrid</code> has changed since {@link #paint(Graphics)} was last
     * changed, <code>false</code> otherwise.
     *
     * @return  <code>true</code>if the <code>CreditGrid</code> has changed since {@link #paint(Graphics)} was last
     *          changed, <code>false</code> otherwise.
     */
    public boolean isChanged() {
        return changed;
    }

    /**
     * Instantiates the {@link CreditCell} array (but not the <code>CreditCell</code> objects themselves) with the
     * desired dimensions. Optimises the size of the <code>CreditCell</code> objects in accordance with the default
     * margins ({@link #defMarginX} and {@link #defMarginY}).
     *
     * @param dimX  the desired number of columns.
     * @param dimY  the desired number of rows.
     */
    private void initCells(int dimX, int dimY) {
        dimX = Math.max(dimX, 2);
        dimY = Math.max(dimY, 2);
        int maxCellSizeX = (Main.width - 2*defMarginX) / dimX;
        int maxCellSizeY = (Main.height - 2*defMarginY) / dimY;
        setCellSize(Math.min(maxCellSizeX, maxCellSizeY));
        this.cells = new CreditCell[dimX][dimY];
        genCells();
    }

    /**
     * Instantiates the {@link CreditCell} array (but not the <code>CreditCell</code> objects themselves) with the
     * desired dimensions and size of the <code>CreditCell</code> objects.
     *
     * @param dimX      the desired number of columns.
     * @param dimY      the desired number of rows.
     * @param cellSize  the desired size of each {@link CreditCell}.
     */
    private void initCells(int dimX, int dimY, int cellSize) {
        dimX = Math.max(dimX, 2);
        dimY = Math.max(dimY, 2);
        setCellSize(cellSize);
        this.cells = new CreditCell[dimX][dimY];
        genCells();
    }

    /**
     * Instantiates each {@link CreditCell} in the <code>CreditGrid</code>.
     */
    private void genCells() {
        for (int x=0; x<cells.length; x++) {
            for (int y=0; y<cells[0].length; y++) {
                int cellPosX = posX + x*cellSize;
                int cellPosY = posY + y*cellSize;
                int rand = random.nextInt(4);
                switch (rand) {
                    case 0:
                        cells[x][y] = new Rock(cellPosX, cellPosY);
                        break;
                    case 1:
                        cells[x][y] = new Dirt(cellPosX, cellPosY);
                        break;
                    case 2:
                        cells[x][y] = new Grass(cellPosX, cellPosY);
                        break;
                    case 3:
                        cells[x][y] = new Trees(cellPosX, cellPosY);
                        break;
                }
            }
        }
    }

    /**
     * Sets the side length of each {@link CreditCell} in the <code>CreditGrid</code>. Should not be called from outside
     * the constructors.
     *
     * @param cellSize  the desired side length of each <code>CreditCell</code>.
     */
    private void setCellSize(int cellSize) {
        this.cellSize = cellSize;
        if (cells != null) {
            changed = true;
        }
    }

    /**
     * Sets the position of the <code>CreditGrid</code>, interpreted as the top-left corner. Should not be called from
     * outside the constructors.
     *
     * @param posX  the desired x-position of the <code>CreditGrid</code>.
     * @param posY  the desired y-position of the <code>CreditGrid</code>.
     */
    private void setPos(int posX, int posY) {
        this.posX = posX;
        this.posY = posY;
        if (cells != null) {
            changed = true;
        }
    }

}