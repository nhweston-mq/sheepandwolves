/*
 * COMP229 A1
 * Nicholas Weston (44882017)
 */

package sheepandwolves;

import java.awt.*;

/**
 * Represents a grass {@link CreditCell} in a {@link CreditGrid}.
 *
 * @author  Nicholas Weston
 */
@Deprecated
public class Grass extends CreditCell {

    /**
     * The colour of grass terrain.
     */
    protected static final Color color = new Color(128, 255, 0);

    /**
     * Creates a new terrain at the specified position, interpreted as the top-left corner of the <code>Grass</code>.
     *
     * @param posX  the desired x-position of the <code>Grass</code>.
     * @param posY  the desired y-position of the <code>Grass</code>.
     */
    public Grass(int posX, int posY) {
        super(posX, posY);
    }

    /**
     * Method to draw the <code>Grass</code> object on the specified {@link Graphics} with the specified size.
     *
     * @param g     the <code>Graphics</code> to draw the <code>Grass</code> on.
     * @param size  the side length of the <code>Grass</code> to be drawn.
     */
    @Override
    public void paint(Graphics g, int size) {
        g.setColor(color);
        g.fillRect(getPosX(), getPosY(), size, size);
        g.setColor(Main.colStroke);
        g.drawRect(getPosX(), getPosY(), size, size);
    }

}
