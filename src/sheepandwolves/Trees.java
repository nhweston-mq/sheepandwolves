/*
 * COMP229 A1
 * Nicholas Weston (44882017)
 */

package sheepandwolves;

import java.awt.*;

/**
 * Represents a trees {@link CreditCell} in a {@link CreditGrid}.
 *
 * @author  Nicholas Weston
 */
@Deprecated
public class Trees extends CreditCell {

    /**
     * The colour of trees terrain.
     */
    protected static Color color = new Color(0, 128, 64);

    /**
     * Creates a new terrain at the specified position, interpreted as the top-left corner of the <code>Trees</code>.
     *
     * @param posX  the desired x-position of the <code>Trees</code>.
     * @param posY  the desired y-position of the <code>Trees</code>.
     */
    public Trees(int posX, int posY) {
        super(posX, posY);
    }

    /**
     * Method to draw the <code>Trees</code> object on the specified {@link Graphics} with the specified size.
     *
     * @param g     the <code>Graphics</code> to draw the <code>Trees</code> on.
     * @param size  the side length of the <code>Trees</code> to be drawn.
     */
    @Override
    public void paint(Graphics g, int size) {
        g.setColor(color);
        g.fillRect(getPosX(), getPosY(), size, size);
        g.setColor(Main.colStroke);
        g.drawRect(getPosX(), getPosY(), size, size);
    }

}
