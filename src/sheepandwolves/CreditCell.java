/*
 * COMP229 A1
 * Nicholas Weston (44882017)
 */

package sheepandwolves;

import java.awt.*;

/**
 * Represents a cell in a {@link CreditGrid}, providing methods for its I/O and functionality.
 *
 * @author  Nicholas Weston
 */
@Deprecated
public class CreditCell {

    private int posX, posY;

    /**
     * Creates a new <code>CreditCell</code> at the specified position.
     *
     * @param posX  the desired x-position of the <code>CreditCell</code>.
     * @param posY  the desired y-position of the <code>CreditCell</code>.
     */
    public CreditCell(int posX, int posY) {
        setPos(posX, posY);
    }

    /**
     * Method to draw the <code>CreditCell</code> on the specified {@link Graphics} with the specified side length.
     * This method does nothing and is to be overridden by the subclasses.
     *
     * @param g     the <code>Graphics</code> to draw the <code>CreditCell</code> to.
     * @param size  the side length of the <code>CreditCell</code> to be drawn.
     */
    public void paint(Graphics g, int size) {}

    /**
     * Returns the x-position of the top-left corner of the <code>CreditCell</code>.
     *
     * @return  the x-position of the <code>CreditCell</code>.
     */
    public int getPosX() {
        return posX;
    }

    /**
     * Returns the y-position of the top-left corner of the <code>CreditCell</code>.
     *
     * @return  the y-position of the <code>CreditCell</code>.
     */
    public int getPosY() {
        return posY;
    }

    /**
     * Sets the position of the <code>CreditCell</code> to the specified position, interpreted as the top-left corner.
     * Should not be called from outside the constructors.
     *
     * @param posX  the desired x-position of the <code>CreditCell</code>.
     * @param posY  the desired y-position of the <code>CreditCell</code>.
     */
    private void setPos(int posX, int posY) {
        this.posX = posX;
        this.posY = posY;
    }

}
