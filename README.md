# Distinction-Level Discussion

The Credit solution is worse than the Pass solution. The Credit solution uses subclasses of `CreditCell` to represent
each type of terrain. This approach is unnecessary, needlessly overcomplicates the code, and reduces scalability. The
Pass solution uses a field to denote the terrain type. This is far more efficient, simple, and scalable. The specific
reasons for this are detailed in the following discussion.

## Needlessness

The only effective difference between the subclasses of `CreditCell` is the static `color` variables. It is not
reasonable to be creating distinct classes only to distinguish between a single property. The difference between the
colours of the terrain types can be accounted for simply with an instance variable representing the type of terrain,
and some form of association between terrain types and their respective colours.

In my implementation of the Pass section, I used an enum, `Terrain`, to represent terrain types. Each `Terrain` has a
`Color` value as a field. Hence the colour of a `Cell` can be accessed (within the class) simply with
`terrain.getColor()`. There is no reason to be representing different terrain types with different subclasses. Such an
approach has no intrinsic advantage to using a field. The question henceforth, is how the two approaches compare in
their overheads.

## Redundancy

A major difference between the overheads of the Pass and Credit solutions is redundancy. Notice that the subclasses of
`CreditCell` are extremely repetitive. As mentioned previously, the only difference between the subclasses is the
colour (and trivially, the class names). Everything else, including the `paint()` method (note that the Credit
specifications require that this be overridden in each subclass), are exactly the same. Even if the `paint()` method
were not overridden in the subclasses, there would still be four additional files, each with a constructor. This is
redundancy, and is therefore poor coding practice.

The alternative, the Pass solution, avoids redundancy. By using a field to represent terrain type, the Pass solution
does not repeat class declarations, the `paint()` method, or the constructor, all of which are impertinent to the
distinction of terrain colour.

## Lack of Scalability

Another problem with the Credit solution is that it lacks scalability and will increase the difficulty of adding new
features and functionality. If new terrain types are to be introduced, a new class will have to be created for every
new terrain type added. This compounds the redundancy problem, necessitating that the code become increasingly
repetitive and redundant as new features are included. Furthermore, if a future version of the program were to involve
the potential for a `Cell` to change terrain type, it would be far more difficult to implement since a change of class
is far more problematic than a change in an instance variable. Changing class will require that the `Cell` be
reinstantiated as the new subclass, with all its other variables copied over from the previous instance.

Conversely, adding new terrain types to the Pass level solution would be a mere matter of expanding the values of the
`Terrain` enum. This, for each new terrain type, adds only one line of additional code to the entire program, as
opposed to a new 50-line file. \(One that is created mostly through copying, pasting and replacing!\) Changing terrain
type with the Pass level solution would involve a setter, which is unconvoluted and inexpensive. 

As a final example of the lack of scalability, consider a future version of the program involving a more dynamic
terrain system. For instance, terrain types might extend beyond a one-dimensional typological system, where a terrain
could be grassy, rocky, muddy, forested, or *any combination* of the aforementioned. Such a system would yield 16 \(or
15 if we exclude the null case\) distinct terrain 'types'. Another possible extension is that the types become
non-binary values, in other words, the `Cell` instances are assigned *degrees* of grassiness, rockiness, etc. This
would yield an astronomical number of possible terrains. There is no way the Credit solution could be reasonably
extrapolated and extended in this case.

In the Pass level solution, such a dynamic terrain system as those previously discussed could be implemented through a
`Map` instance variable, with the `Terrain` enums as keys and `Boolean`, `Double` or `Integer` as values. Such an
implementation would require little overall change to the code.

All of these cases not only exemplify the lack of scalability in the Credit approach, but also demonstrate *reductio
ad absurdum* why distinguishing between classes for the sake of what could simply be an enum instance variable is poor
programming practice. In essence, classes should distinguish between functionally distinct entities. Cells of different
terrain types are functionally identical, and should not be separate classes.

## Conclusion

The Pass level solution is far superior to the Credit level solution because it minimises repetition and redundancy in
the code and facilitates a high degree of scalability. The Credit solution carries no advantages over the Pass
solution. It only serves to clutter and needlessly convolute code \(as well as the source code directory\) to
distinguish between a property that could have been an instance variable. The redundancy and inefficiency of the Credit
solution will only compound and worsen if new features \(related to cells and terrain\) are added. It is best that
non-functional differences between entities represented in an object-oriented program be expressed through fields, not
new classes.